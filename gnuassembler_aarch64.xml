<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE language SYSTEM "language.dtd">

<!--
    Copyright 2020 Jackson Harmer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

<language name="GNU Assembler (aarch64)" version="1" kateversion="5.0" section="Assembler" extensions="*.s;*.S" mimetype="text/x-asm" author="Jackson Harmer (jharmer95@gmail.com)" license="GPLv3">
    <highlighting>
    <list name="keywords">
      <item>.abort</item>
      <item>.align</item>
      <item>.app-file</item>
      <item>.appline</item>
      <item>.ascii</item>
      <item>.asciz</item>
      <item>.att_syntax</item>
      <item>.balign</item>
      <item>.balignl</item>
      <item>.balignw</item>
      <item>.byte</item>
      <item>.code16</item>
      <item>.code32</item>
      <item>.comm</item>
      <item>.common.s</item>
      <item>.common</item>
      <item>.data</item>
      <item>.dc.b</item>
      <item>.dc.d</item>
      <item>.dc.l</item>
      <item>.dc.s</item>
      <item>.dc.w</item>
      <item>.dc.x</item>
      <item>.dc</item>
      <item>.dcb.b</item>
      <item>.dcb.d</item>
      <item>.dcb.l</item>
      <item>.dcb.s</item>
      <item>.dcb.w</item>
      <item>.dcb.x</item>
      <item>.dcb</item>
      <item>.debug</item>
      <item>.def</item>
      <item>.desc</item>
      <item>.dim</item>
      <item>.double</item>
      <item>.ds.b</item>
      <item>.ds.d</item>
      <item>.ds.l</item>
      <item>.ds.p</item>
      <item>.ds.s</item>
      <item>.ds.w</item>
      <item>.ds.x</item>
      <item>.ds</item>
      <item>.dsect</item>
      <item>.eject</item>
      <item>.else</item>
      <item>.elsec</item>
      <item>.elseif</item>
      <item>.end</item>
      <item>.endc</item>
      <item>.endef</item>
      <item>.endfunc</item>
      <item>.endif</item>
      <item>.endm</item>
      <item>.endr</item>
      <item>.equ</item>
      <item>.equiv</item>
      <item>.err</item>
      <item>.exitm</item>
      <item>.extend</item>
      <item>.extern</item>
      <item>.fail</item>
      <item>.file</item>
      <item>.fill</item>
      <item>.float</item>
      <item>.format</item>
      <item>.func</item>
      <item>.global</item>
      <item>.globl</item>
      <item>.hidden</item>
      <item>.hword</item>
      <item>.ident</item>
      <item>.if</item>
      <item>.ifc</item>
      <item>.ifdef</item>
      <item>.ifeq</item>
      <item>.ifeqs</item>
      <item>.ifge</item>
      <item>.ifgt</item>
      <item>.ifle</item>
      <item>.iflt</item>
      <item>.ifnc</item>
      <item>.ifndef</item>
      <item>.ifne</item>
      <item>.ifnes</item>
      <item>.ifnotdef</item>
      <item>.include</item>
      <item>.int</item>
      <item>.intel_syntax</item>
      <item>.internal</item>
      <item>.irep</item>
      <item>.irepc</item>
      <item>.irp</item>
      <item>.irpc</item>
      <item>.lcomm</item>
      <item>.lflags</item>
      <item>.line</item>
      <item>.linkonce</item>
      <item>.list</item>
      <item>.llen</item>
      <item>.ln</item>
      <item>.long</item>
      <item>.lsym</item>
      <item>.macro</item>
      <item>.mexit</item>
      <item>.name</item>
      <item>.noformat</item>
      <item>.nolist</item>
      <item>.nopage</item>
      <item>noprefix</item>
      <item>.octa</item>
      <item>.offset</item>
      <item>.org</item>
      <item>.p2align</item>
      <item>.p2alignl</item>
      <item>.p2alignw</item>
      <item>.page</item>
      <item>.plen</item>
      <item>.popsection</item>
      <item>.previous</item>
      <item>.print</item>
      <item>.protected</item>
      <item>.psize</item>
      <item>.purgem</item>
      <item>.pushsection</item>
      <item>.quad</item>
      <item>.rodata</item>
      <item>.rep</item>
      <item>.rept</item>
      <item>.rva</item>
      <item>.sbttl</item>
      <item>.scl</item>
      <item>.sect.s</item>
      <item>.sect</item>
      <item>.section.s</item>
      <item>.section</item>
      <item>.set</item>
      <item>.short</item>
      <item>.single</item>
      <item>.size</item>
      <item>.skip</item>
      <item>.sleb128</item>
      <item>.space</item>
      <item>.spc</item>
      <item>.stabd</item>
      <item>.stabn</item>
      <item>.stabs</item>
      <item>.string</item>
      <item>.struct</item>
      <item>.subsection</item>
      <item>.symver</item>
      <item>.tag</item>
      <item>.text</item>
      <item>.title</item>
      <item>.ttl</item>
      <item>.type</item>
      <item>.uleb128</item>
      <item>.use</item>
      <item>.val</item>
      <item>.version</item>
      <item>.vtable_entry</item>
      <item>.vtable_inherit</item>
      <item>.weak</item>
      <item>.word</item>
      <item>.xcom</item>
      <item>.xdef</item>
      <item>.xref</item>
      <item>.xstabs</item>
      <item>.zero</item>
      <!-- Directives specific to ARM -->
      <item>.arm</item>
      <item>.bss</item>
      <item>.code</item>
      <item>.even</item>
      <item>.force_thumb</item>
      <item>.ldouble</item>
      <item>.loc</item>
      <item>.ltorg</item>
      <item>.packed</item>
      <item>.pool</item>
      <item>.req</item>
      <item>.thumb</item>
      <item>.thumb_func</item>
      <item>.thumb_set</item>
    </list>
    
    <list name="aliases">
        <item>adc</item>
        <item>adcs</item>
        <item>add</item>
        <item>adds</item>
        <item>addg</item>
        <item>adr</item>
        <item>adr</item>
        <item>adrp</item>
        <item>and</item>
        <item>ands</item>
        <item>asr</item>
        <item>asrv</item>
        <item>at</item>
        <item>autda</item>
        <item>autdza</item>
        <item>autdb</item>
        <item>autdzb</item>
        <item>autia</item>
        <item>autia1716</item>
        <item>autiasp</item>
        <item>autiaz</item>
        <item>autiza</item>
        <item>autib</item>
        <item>autib1716</item>
        <item>autibsp</item>
        <item>autibz</item>
        <item>autizb</item>
        <item>axflag</item>
        <item>b</item>
        <item>b.eq</item>
        <item>b.ne</item>
        <item>b.cs</item>
        <item>b.hs</item>
        <item>b.cc</item>
        <item>b.lo</item>
        <item>b.mi</item>
        <item>b.pl</item>
        <item>b.vs</item>
        <item>b.vc</item>
        <item>b.hi</item>
        <item>b.ls</item>
        <item>b.ge</item>
        <item>b.lt</item>
        <item>b.gt</item>
        <item>b.le</item>
        <item>b.al</item>
        <item>bfc</item>
        <item>bfi</item>
        <item>bfm</item>
        <item>bfxil</item>
        <item>bic</item>
        <item>bics</item>
        <item>bl</item>
        <item>blr</item>
        <item>blraa</item>
        <item>blraaz</item>
        <item>blrab</item>
        <item>blrabz</item>
        <item>br</item>
        <item>braa</item>
        <item>braaz</item>
        <item>brab</item>
        <item>brabz</item>
        <item>brk</item>
        <item>bti</item>
        <item>cas</item>
        <item>casa</item>
        <item>casal</item>
        <item>casl</item>
        <item>casb</item>
        <item>casab</item>
        <item>casalb</item>
        <item>caslb</item>
        <item>cash</item>
        <item>casah</item>
        <item>casalh</item>
        <item>caslh</item>
        <item>casp</item>
        <item>caspa</item>
        <item>caspal</item>
        <item>caspl</item>
        <item>cbnz</item>
        <item>cbz</item>
        <item>ccmn</item>
        <item>ccmp</item>
        <item>cfinv</item>
        <item>cfp</item>
        <item>cinc</item>
        <item>cinv</item>
        <item>clrex</item>
        <item>cls</item>
        <item>clz</item>
        <item>cmn</item>
        <item>cmp</item>
        <item>cmpp</item>
        <item>cneg</item>
        <item>cpp</item>
        <item>crc32b</item>
        <item>crc32h</item>
        <item>crc32w</item>
        <item>crc32x</item>
        <item>crc32cb</item>
        <item>crc32ch</item>
        <item>crc32cw</item>
        <item>crc32cx</item>
        <item>csdb</item>
        <item>csel</item>
        <item>cset</item>
        <item>csetm</item>
        <item>csinc</item>
        <item>csinv</item>
        <item>csneg</item>
        <item>dc</item>
        <item>dcps1</item>
        <item>dcps2</item>
        <item>dcps3</item>
        <item>dmb</item>
        <item>drps</item>
        <item>dsb</item>
        <item>dvp</item>
        <item>eon</item>
        <item>eor</item>
        <item>eret</item>
        <item>eretaa</item>
        <item>eretab</item>
        <item>esb</item>
        <item>extr</item>
        <item>gmi</item>
        <item>hint</item>
        <item>hlt</item>
        <item>hvc</item>
        <item>ic</item>
        <item>irg</item>
        <item>isb</item>
        <item>ldadd</item>
        <item>ldadda</item>
        <item>ldaddal</item>
        <item>ldaddl</item>
        <item>ldaddb</item>
        <item>ldaddab</item>
        <item>ldaddalb</item>
        <item>ldaddlb</item>
        <item>ldaddh</item>
        <item>ldaddah</item>
        <item>ldaddalh</item>
        <item>ldaddlh</item>
        <item>ldapr</item>
        <item>ldaprb</item>
        <item>ldaprh</item>
        <item>ldapur</item>
        <item>ldapurb</item>
        <item>ldapurh</item>
        <item>ldapursb</item>
        <item>ldapursh</item>
        <item>ldapursw</item>
        <item>ldar</item>
        <item>ldarb</item>
        <item>ldarh</item>
        <item>ldaxp</item>
        <item>ldaxr</item>
        <item>ldaxrb</item>
        <item>ldaxrh</item>
        <item>ldclr</item>
        <item>ldclra</item>
        <item>ldclral</item>
        <item>ldclrl</item>
        <item>ldclrb</item>
        <item>ldclrab</item>
        <item>ldclralb</item>
        <item>ldclrlb</item>
        <item>ldclrh</item>
        <item>ldclrah</item>
        <item>ldclralh</item>
        <item>ldclrlh</item>
        <item>ldeor</item>
        <item>ldeora</item>
        <item>ldeoral</item>
        <item>ldeorl</item>
        <item>ldeorb</item>
        <item>ldeorab</item>
        <item>ldeoralb</item>
        <item>ldeorlb</item>
        <item>ldeorh</item>
        <item>ldeorah</item>
        <item>ldeoralh</item>
        <item>ldeorlh</item>
        <item>ldg</item>
        <item>ldgv</item>
        <item>ldlar</item>
        <item>ldlarb</item>
        <item>ldlarh</item>
        <item>ldnp</item>
        <item>ldp</item>
        <item>ldpsw</item>
        <item>ldr</item>
        <item>ldraa</item>
        <item>ldrab</item>
        <item>ldrb</item>
        <item>ldrh</item>
        <item>ldrsb</item>
        <item>ldrsh</item>
        <item>ldrsw</item>
        <!-- TODO: Finish standard aliases -->
        <item>mov</item>
        <item>movk</item>
        <item>movn</item>
        <item>movz</item>
        <item>nop</item>
        <item>sub</item>
        <item>subs</item>
        <item>svc</item>
        <item>tst</item>
    </list>
    
    <list name="sp_aliases">
        <item>abs</item>
        <item>add</item>
        <item>addhn</item>
        <item>addhn2</item>
        <item>addp</item>
        <item>addv</item>
        <item>aesd</item>
        <item>aese</item>
        <item>aesimc</item>
        <item>aesmc</item>
        <item>and</item>
        <item>bcax</item>
        <item>bic</item>
        <!-- TODO: Finish NEON/FPU aliases -->
    </list>
    
    <list name="registers">
        <item>w0</item>
        <item>w1</item>
        <item>w2</item>
        <item>w3</item>
        <item>w4</item>
        <item>w5</item>
        <item>w6</item>
        <item>w7</item>
        <item>w8</item>
        <item>w9</item>
        <item>w10</item>
        <item>w11</item>
        <item>w12</item>
        <item>w13</item>
        <item>w14</item>
        <item>w15</item>
        <item>w16</item>
        <item>w17</item>
        <item>w18</item>
        <item>w19</item>
        <item>w20</item>
        <item>w21</item>
        <item>w22</item>
        <item>w23</item>
        <item>w24</item>
        <item>w25</item>
        <item>w26</item>
        <item>w27</item>
        <item>w28</item>
        <item>w29</item>
        <item>w30</item>
        <item>x0</item>
        <item>x1</item>
        <item>x2</item>
        <item>x3</item>
        <item>x4</item>
        <item>x5</item>
        <item>x6</item>
        <item>x7</item>
        <item>x8</item>
        <item>x9</item>
        <item>x10</item>
        <item>x11</item>
        <item>x12</item>
        <item>x13</item>
        <item>x14</item>
        <item>x15</item>
        <item>x16</item>
        <item>x17</item>
        <item>x18</item>
        <item>x19</item>
        <item>x20</item>
        <item>x21</item>
        <item>x22</item>
        <item>x23</item>
        <item>x24</item>
        <item>x25</item>
        <item>x26</item>
        <item>x27</item>
        <item>x28</item>
        <item>x29</item>
        <item>x30</item>
        <item>pc</item>
        <item>sp</item>
        <item>lr</item>
        <item>xzr</item>
    </list>

    <contexts>
      <context attribute="Normal Text" lineEndContext="#stay" name="Normal">
        <RegExpr      attribute="Label" context="#stay" String="[_\w\d-]*\s*:" />
        <keyword      attribute="Keyword" context="#stay" String="keywords"/>
        <keyword      attribute="Alias" context="#stay" String="aliases"/>
        <keyword      attribute="SpecialAlias" context="#stay" String="sp_aliases"/>
        <keyword      attribute="Register" context="#stay" String="registers"/>
        <HlCOct       attribute="Octal" context="#stay" />
        <HlCHex       attribute="Hex" context="#stay" />
        <RegExpr      attribute="Binary" context="#stay" String="0[bB][01]+" />
        <Int          attribute="Decimal" context="#stay" />
        <RegExpr      attribute="Float" context="#stay" String="0[fFeEdD][-+]?[0-9]*\.?[0-9]*[eE]?[-+]?[0-9]+" />
        <RegExpr      attribute="Normal Text" context="#stay" String="[A-Za-z_.$][A-Za-z0-9_.$]*" />
        <HlCChar      attribute="Char" context="#stay" />
        <RegExpr      attribute="Char" context="#stay" String="'(\\x[0-9a-fA-F][0-9a-fA-F]?|\\[0-7]?[0-7]?[0-7]?|\\.|.)" />
        <DetectChar   attribute="String" context="String" char="&quot;" />
        <RegExpr      attribute="Preprocessor" context="Preprocessor" String="#\s*if(?:def|ndef)?(?=\s+\S)" insensitive="true" beginRegion="PP" firstNonSpace="true" />
        <RegExpr      attribute="Preprocessor" context="Preprocessor" String="#\s*endif" insensitive="true" endRegion="PP" firstNonSpace="true" />
        <RegExpr      attribute="Preprocessor" context="Define" String="#\s*define.*((?=\\))" insensitive="true" firstNonSpace="true" />
        <RegExpr      attribute="Preprocessor" context="Preprocessor" String="#\s*(?:el(?:se|if)|include(?:_next)?|define|undef|line|error|warning|pragma)" firstNonSpace="true" />
        <RegExpr      attribute="Immediate" context="Immediate" String="#[0-9\(']" />
        <Detect2Chars attribute="Comment" context="Comment Type 1" char="/" char1="*" />
        <Detect2Chars attribute="Comment" context="Comment Type 2" char="/" char1="/" />
        <AnyChar      attribute="Symbol" context="#stay" String="!#%&amp;*()+,-&lt;=&gt;?/:[]^{|}~" />
      </context>
      <context attribute="Comment" lineEndContext="#stay" name="Comment Type 1">
        <IncludeRules context="##Alerts" />
        <Detect2Chars attribute="Comment" context="#pop" char="*" char1="/" />
      </context>
      <context attribute="Comment" lineEndContext="#pop" name="Comment Type 2" >
        <IncludeRules context="##Alerts" />
      </context>
      <context attribute="Immediate" lineEndContext="#pop" name="Immediate">
          <DetectChar attribute="Immediate" context="#pop" char="," />
      </context>
      <context attribute="String" lineEndContext="#pop" name="String">
        <LineContinue  attribute="String" context="Some Context" />
        <HlCStringChar attribute="String Char" context="#stay" />
        <DetectChar    attribute="String" context="#pop" char="&quot;" />
      </context>
      <context attribute="Preprocessor" lineEndContext="#pop" name="Preprocessor" />
      <context attribute="Preprocessor" lineEndContext="#pop" name="Define">
        <LineContinue attribute="Preprocessor" context="#stay"/>
      </context>
      <context attribute="Normal Text" lineEndContext="#pop" name="Some Context" />
    </contexts>

    <itemDatas>
      <itemData name="Normal Text"  defStyleNum="dsNormal"   />
      <itemData name="Label"        defStyleNum="dsKeyword"  />
      <itemData name="Keyword"      defStyleNum="dsKeyword"  />
      <itemData name="Alias"        defStyleNum="dsFunction" />
      <itemData name="SpecialAlias" defStyleNum="dsExtension" />
      <itemData name="Register"     defStyleNum="dsBuiltIn" />
      <itemData name="Decimal"      defStyleNum="dsDecVal"   />
      <itemData name="Octal"        defStyleNum="dsBaseN"    />
      <itemData name="Hex"          defStyleNum="dsBaseN"    />
      <itemData name="Binary"       defStyleNum="dsBaseN"    />
      <itemData name="Float"        defStyleNum="dsFloat"    />
      <itemData name="Char"         defStyleNum="dsChar"     />
      <itemData name="String"       defStyleNum="dsString"   />
      <itemData name="String Char"  defStyleNum="dsSpecialChar" />
      <itemData name="Symbol"       defStyleNum="dsNormal"   />
      <itemData name="Comment"      defStyleNum="dsComment"  />
      <itemData name="Preprocessor" defStyleNum="dsPreprocessor" />
      <itemData name="Immediate"    defStyleNum="dsConstant" />
    </itemDatas>
  </highlighting>

  <general>
    <comments>
        <comment name="singleLine" start="//" />
        <comment name="multiLine"  start="/*" end="*/" />
    </comments>
    <keywords casesensitive="0" weakDeliminator="_.$" />
  </general>
</language>
